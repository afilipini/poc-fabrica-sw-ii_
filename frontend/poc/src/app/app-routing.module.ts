import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'consulta', pathMatch: 'full' },
  { path: 'consulta', loadChildren: './pages/consulta/consulta.module#ConsultaModule' },
  { path: 'cadastro/:id', loadChildren: './pages/cadastro/cadastro.module#CadastroModule' },
  { path: 'relatorio', loadChildren: './pages/relatorio/relatorio.module#RelatorioModule' }
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }