import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Orgao } from '../models/classes/Orgao';
import { Observable } from 'rxjs';
import { BaseAPI } from '../utils/BaseAPI';
import { ContagemRelatorioOrgaoPorStatusDTO } from '../models/dto/ContagemRelatorioOrgaoPorStatusDTO';


@Injectable({
  providedIn: 'root'
})
export class OrgaoService {

  private ResourcePath: string = "/orgao";

  constructor(private http: HttpClient) { }


  public salvarOrgao(orgao: Orgao): Observable<String>{
    return this.http.post<String>(BaseAPI.url + this.ResourcePath + "/salvarOrgao", orgao);
  }

  public listarOrgaosPorNome(nome: String): Observable<Array<Orgao>>{
    console.log(BaseAPI.url);
    return this.http.get<Array<Orgao>>(BaseAPI.url + this.ResourcePath + "/listarOrgaosPorNome?nome=" + nome);
  }

  public listarOrgaoPorCodigo(codigo: number): Observable<Orgao>{
    return this.http.get<Orgao>(BaseAPI.url + this.ResourcePath + "/listarOrgaoPorCodigo/" + codigo);
  }

  public listarOrgaosOrdenadosPorStatus(): Observable<Array<Orgao>>{
    return this.http.get<Array<Orgao>>(BaseAPI.url + this.ResourcePath + "/listarOrgaosOrdenadosPorStatus");
  }

  public consolidarContagemRelatorioOrgaoPorStatus(): Observable<Array<ContagemRelatorioOrgaoPorStatusDTO>>{
    return this.http.get<Array<ContagemRelatorioOrgaoPorStatusDTO>>(BaseAPI.url + this.ResourcePath + "/consolidarContagemRelatorioOrgaoPorStatus");
  }


}
