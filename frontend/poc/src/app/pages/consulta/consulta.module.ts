import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ConsultaComponent } from './Consulta.component';

@NgModule({
  declarations: [ConsultaComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: ConsultaComponent
      }
    ])
  ]
})
export class ConsultaModule { }