import { Component, OnInit } from '@angular/core';
import { OrgaoService } from 'src/app/services/orgao.service';
import { Orgao } from 'src/app/models/classes/Orgao';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  orgaos: Array<Orgao> = [];
  nome: string = '';


  constructor(private orgaoService: OrgaoService) { }

  ngOnInit() {
  }


  pesquisarOrgao(){
    console.log(this.nome);
    console.log('this.nome');
    this.orgaoService.listarOrgaosPorNome(this.nome).subscribe(result => {
      this.orgaos = result;
    }, error => {
      console.log(error);
    });
  }

}
