import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RelatorioComponent } from './Relatorio.component';

@NgModule({
  declarations: [RelatorioComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: RelatorioComponent
      }
    ])
  ]
})
export class RelatorioModule { }
