import { Component, OnInit } from '@angular/core';
import { OrgaoService } from 'src/app/services/orgao.service';
import { Orgao } from 'src/app/models/classes/Orgao';
import { ContagemRelatorioOrgaoPorStatusDTO } from 'src/app/models/dto/ContagemRelatorioOrgaoPorStatusDTO';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.css']
})
export class RelatorioComponent implements OnInit {

  orgaos: Array<Orgao> = [];
  orgaosQuantidadeDTO: Array<ContagemRelatorioOrgaoPorStatusDTO> = [];
  quantidadeAtivos: number = 0;
  quantidadeInativos: number = 0;


  constructor(private orgaoService: OrgaoService) { }

  ngOnInit() {
    this.listarOrgaosOrdenados();
    this.listarContagem();
  }


  listarOrgaosOrdenados(){
    this.orgaoService.listarOrgaosOrdenadosPorStatus().subscribe(result => {
      this.orgaos = result;
    }, error => {
      console.log(error);
    });
  }


  listarContagem(){
    this.orgaoService.consolidarContagemRelatorioOrgaoPorStatus().subscribe(result => {
      this.orgaosQuantidadeDTO = result;

      this.orgaosQuantidadeDTO.forEach(element => {
        element.status === 'S' ? this.quantidadeAtivos = element.quantidade : this.quantidadeInativos = element.quantidade;
      });

    }, error => {
      console.log(error);
    });
  }

}
