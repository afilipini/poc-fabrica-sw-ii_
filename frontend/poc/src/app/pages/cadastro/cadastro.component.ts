import { Component, OnInit } from '@angular/core';
import { Orgao } from 'src/app/models/classes/Orgao';

import { OrgaoService } from 'src/app/services/orgao.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Mensagem } from 'src/app/utils/Mensagem';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  orgao = new Orgao();
  mensagem: Mensagem;

  constructor(private orgaoService: OrgaoService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.mensagem = new Mensagem('',false);

    this.route.params.subscribe(params => {
      this.orgaoService.listarOrgaoPorCodigo(params['id']).subscribe(result => {
        this.orgao = result;
      }, error  => {
        console.log(error);
      });
    });
    
  }


  salvarCadastro(){
    this.orgaoService.salvarOrgao(this.orgao).subscribe(result => {
      console.log(result);
      this.mensagem.descricao = result;
      this.mensagem.sucesso = true;

      console.log(this.mensagem);

    }, error => {

      this.mensagem.descricao = error.error;
      this.mensagem.sucesso = false;

    });
  }




}
