export class Orgao{

    codigo: number;
    nome: string;
    sigla: string;
    localidade: string;
    coordenador: string;
    sala: string;
    telefone: string;
    status: string;
    

    constructor(codigo?: number, nome?: string, sigla ?: string, localidade ?: string, coordenador ?: string, sala ?: string, telefone ?: string, status?: string){
        this.codigo = codigo;
        this.nome = nome;
        this.sigla = sigla;
        this.localidade = localidade;
        this.coordenador = coordenador;
        this.sala = sala;
        this.telefone = telefone;
        this.status = status;
    }

}