export class ContagemRelatorioOrgaoPorStatusDTO {

    status: string ;
    quantidade: number;
    

    constructor(status?: string, quantidade?: number){
        status === 'S' ? this.status = 'Ativo' : this.status = 'Inativo';
        this.quantidade = quantidade;
    }

}