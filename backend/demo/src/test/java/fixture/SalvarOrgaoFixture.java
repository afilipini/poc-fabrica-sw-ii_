package fixture;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.runner.RunWith;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.gov.mpdft.domain.entity.Orgao;

@RunWith(ConcordionRunner.class)
public class SalvarOrgaoFixture {

	public Orgao salvarOrgao(Long codigoOrgao, String nomeOrgao, String siglaOrgao, String localidadeOrgao,
			String coordenadorOrgao, Integer salaOrgao, String telefoneOrgao, String statusOrgao) {

		try {
			URL url = new URL("http://localhost:8080/demo/api/v1/orgao/salvarOrgao");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("charset", "utf-8");
			conn.setUseCaches(false);
			String str = "{\"codigo\": " + codigoOrgao + ",\"nome\": \"" + nomeOrgao + "\",\"sigla\": \"" + siglaOrgao
					+ "\",\"localidade\": \"" + localidadeOrgao + "\",\"coordenador\": \"" + coordenadorOrgao
					+ "\",\"sala\": " + salaOrgao + ",\"telefone\":\"" + telefoneOrgao + "\",\"status\":\""
					+ statusOrgao + "\"}";
			conn.setRequestProperty("Content-Length", "" + str.length());
			try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
				wr.write(str.getBytes("utf-8"));
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			url = new URL("http://localhost:8080/demo/api/v1/orgao/listarOrgaoPorCodigo/" + codigoOrgao);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			Gson gson = new GsonBuilder().disableInnerClassSerialization().setDateFormat("yyyy-MM-dd HH:mm:ss")
					.create();
			return gson.fromJson(br.readLine(), Orgao.class);

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return null;
	}

}