package fixture;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.runner.RunWith;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.gov.mpdft.domain.entity.Orgao;

@RunWith(ConcordionRunner.class)
public class ConsultarOrgaoFixture {

	public Iterable<Orgao> listarOrgaosPorNome(String nomeOrgao) {

		try {

			URL url = new URL("http://localhost:8080/demo/api/v1/orgao/listarOrgaosPorNome?nome=" + nomeOrgao);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			Gson gson = new GsonBuilder().disableInnerClassSerialization().setDateFormat("yyyy-MM-dd HH:mm:ss")
					.create();
			Orgao[] s = gson.fromJson(br.readLine(), Orgao[].class);

			conn.disconnect();

			return Arrays.asList(s);

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return null;
	}

}