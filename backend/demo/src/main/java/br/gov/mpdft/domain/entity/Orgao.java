package br.gov.mpdft.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ORGAO")
public class Orgao {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDORGAO", unique=true, nullable=false)
	private Long codigo;
	
	@Column(name="NMORGAO", nullable=false, length=250)
	private String nome;
	
	@Column(name="SGORGAO", nullable=false, length=30)
	private String sigla;
	
	@Column(name="DSLOCALIDADE", nullable=false, length=200)
	private String localidade;
	
	@Column(name="NMCOORDENADOR", nullable=true, length=250)
	private String coordenador;
	
	@Column(name="NRSALA", nullable=true)
	private Integer sala;
	
	@Column(name="NRTELEFONE", nullable=true, length=15)
	private String telefone;
	
	@Column(name="STATIVO", nullable=false, length=1)
	private String status;
	
	
	public Orgao() {
	
	}


	public Orgao(Long codigo, String nome, String sigla, String localidade, String coordenador, Integer sala,
			String telefone, String status) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.sigla = sigla;
		this.localidade = localidade;
		this.coordenador = coordenador;
		this.sala = sala;
		this.telefone = telefone;
		this.status = status;
	}


	public Long getCodigo() {
		return codigo;
	}


	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getSigla() {
		return sigla;
	}


	public void setSigla(String sigla) {
		this.sigla = sigla;
	}


	public String getLocalidade() {
		return localidade;
	}


	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}


	public String getCoordenador() {
		return coordenador;
	}


	public void setCoordenador(String coordenador) {
		this.coordenador = coordenador;
	}


	public Integer getSala() {
		return sala;
	}


	public void setSala(Integer sala) {
		this.sala = sala;
	}


	public String getTelefone() {
		return telefone;
	}


	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Orgao other = (Orgao) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Orgao [codigo=" + codigo + ", nome=" + nome + ", sigla=" + sigla + ", localidade=" + localidade
				+ ", coordenador=" + coordenador + ", sala=" + sala + ", telefone=" + telefone + ", status=" + status
				+ "]";
	}

	
	
	
	
}
