package br.gov.mpdft.domain.dto;

public class ContagemRelatorioOrgaoPorStatusDTO {

	
	private String status;
	private long quantidade;
	
	
	public ContagemRelatorioOrgaoPorStatusDTO() {
	
	}
	
	public ContagemRelatorioOrgaoPorStatusDTO(String status, long quantidade) {
		this.status = status;
		this.quantidade = quantidade;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(long quantidade) {
		this.quantidade = quantidade;
	}
	

	
}
