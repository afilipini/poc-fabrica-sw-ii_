package br.gov.mpdft.rest.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.google.gson.Gson;

import br.gov.mpdft.business.service.OrgaoService;
import br.gov.mpdft.domain.dto.ContagemRelatorioOrgaoPorStatusDTO;
import br.gov.mpdft.domain.entity.Orgao;

@RestController
@RequestMapping("/api/v1/orgao")
public class OrgaoResource {

	
	
	@Autowired
	private OrgaoService orgaoService;
	
	
	
	@GetMapping("/listarOrgaosPorNome")
	public List<Orgao> listarOrgaosPorNome(@RequestParam("nome") String nome){
		return orgaoService.listarOrgaosPorNome(nome);	
	}
	
	@GetMapping("/listarOrgaoPorCodigo/{codigo}")
	public Orgao listarOrgaoPorCodigo(@PathVariable("codigo") Long codigo){
		
		try {
			
			return orgaoService.listarOrgaoPorCodigo(codigo);
			
		} catch (Exception e) {

			throw new ResponseStatusException(
			          HttpStatus.NOT_FOUND, "Registro inexistente", e);
			
		}
		
	}
	
	@PostMapping("/salvarOrgao")
	public ResponseEntity<String>  salvarOrgao(@RequestBody Orgao orgao){
		
		try {
			
			orgaoService.salvarOrgao(orgao);
			
			return ResponseEntity.ok(new Gson().toJson("Órgão salvo com sucesso!"));
			
		} catch (Exception e) {

			return ResponseEntity.badRequest().body(new Gson().toJson(e.getMessage()));
			
		}
		
	}
	
	
	@GetMapping("/listarOrgaosOrdenadosPorStatus")
	public List<Orgao> listarOrgaosOrdenadosPorStatus(){
		
		return orgaoService.listarOrgaosOrdenadosPorStatus();
		
	}
	
	
	
	@GetMapping("/consolidarContagemRelatorioOrgaoPorStatus")
	public List<ContagemRelatorioOrgaoPorStatusDTO> consolidarContagemRelatorioOrgaoPorStatus(){
		
		return orgaoService.consolidarContagemRelatorioOrgaoPorStatus();
		
	}
	
	
}
