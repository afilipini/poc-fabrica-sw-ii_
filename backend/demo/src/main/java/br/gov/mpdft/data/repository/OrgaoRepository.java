package br.gov.mpdft.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.gov.mpdft.domain.dto.ContagemRelatorioOrgaoPorStatusDTO;
import br.gov.mpdft.domain.entity.Orgao;

@Repository
public interface OrgaoRepository extends CrudRepository<Orgao, Long>{

	
	public List<Orgao> findByNomeContaining(String nome);
	
	public List<Orgao> findAllByOrderByStatusDescNomeAsc();

	@Query("select new br.gov.mpdft.domain.dto.ContagemRelatorioOrgaoPorStatusDTO(o.status, count(o.status)) from Orgao o group by o.status")
	public List<ContagemRelatorioOrgaoPorStatusDTO> consolidarContagemRelatorioOrgaoPorStatus();

	
	
	
}
