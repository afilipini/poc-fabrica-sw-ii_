package br.gov.mpdft.business.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.gov.mpdft.data.repository.OrgaoRepository;
import br.gov.mpdft.domain.dto.ContagemRelatorioOrgaoPorStatusDTO;
import br.gov.mpdft.domain.entity.Orgao;

@Service
public class OrgaoService {

	
	@Autowired
	private OrgaoRepository orgaoRepository;
	
	
	
	
	public List<Orgao> listarOrgaosPorNome(String nome){
		return orgaoRepository.findByNomeContaining(nome);	
	}
	

	public Orgao listarOrgaoPorCodigo(Long codigo){
		return orgaoRepository.findById(codigo).get();
	}
	

	public void salvarOrgao(Orgao orgao)throws Exception{
		
		if (orgao.getNome() == null || "".equals(orgao.getNome())) {
			throw new Exception("O nome é obrigatório!");
		}
		if (orgao.getSigla() == null || "".equals(orgao.getSigla())) {
			throw new Exception("A sigla é obrigatória!");
		}
		if (orgao.getLocalidade() == null || "".equals(orgao.getLocalidade())) {
			throw new Exception("A localidade é obrigatória!");
		}
		if (orgao.getStatus() == null || "".equals(orgao.getStatus())) {
			throw new Exception("O status é obrigatório!");
		}
		
		
		orgaoRepository.save(orgao);
		
	}
	
	

	public List<Orgao> listarOrgaosOrdenadosPorStatus(){
		return orgaoRepository.findAllByOrderByStatusDescNomeAsc();
		
	}

	
	
	public List<ContagemRelatorioOrgaoPorStatusDTO> consolidarContagemRelatorioOrgaoPorStatus(){
		return orgaoRepository.consolidarContagemRelatorioOrgaoPorStatus();
	}
	
	
}
